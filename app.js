/**
 * Created by vlad on 10.3.17.
 */

'use strict';

const { WeatherStation, Display } = require('./src');

const weatherStation = new WeatherStation({
  name: 'Global\'s',
  temperature: 20,
  humidity: 65,
  pressure: 745
});

const display1 = new Display({name: 'Minsk'});
const display2 = new Display({name: 'Brest'});
const display3 = new Display({name: 'Grodno'});

weatherStation.addObserver(display1);
weatherStation.addObserver(display2);
weatherStation.addObserver(display3);

weatherStation.setMeasurements({
  temperature: 15,
  humidity: 68,
  pressure: 750
});

console.log('******************************************');

weatherStation.removeObserver(display3);

weatherStation.setMeasurements({
  temperature: 20,
  humidity: 70,
  pressure: 710
});


console.log('******************************************');

