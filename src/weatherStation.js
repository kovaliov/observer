/**
 * Created by vlad on 10.3.17.
 */

'use strict';

const Subject = require('./subject');

function WeatherStation (options) {
  Subject.apply(this, arguments);

  this._name = 'POCO\'s station';

  this._temperature = options.temperature || 0;
  this._humidity = options.humidity || 0;
  this._pressure = options.pressure || 0;

  this.setMeasurements = function (measurements) {
    this._temperature = measurements.temperature;
    this._humidity = measurements.humidity;
    this._pressure = measurements.pressure;

    this.notify({
      temperature: this._temperature,
      humidity: this._humidity,
      pressure: this._pressure
    });
  };
}

WeatherStation.prototype = Object.create(Subject.prototype);
WeatherStation.prototype.constructor = WeatherStation;


module.exports = WeatherStation;
