/**
 * Created by vlad on 10.3.17.
 */
'use strict';

const ObserverList = require('./observerList');

function Subject () {
  this._observers = new ObserverList();
}

Subject.prototype.addObserver = function (observer) {
  this._observers.add(observer);
};

Subject.prototype.removeObserver = function (observer) {
  this._observers.removeAt(this._observers.indexOf(observer, 0));
};

Subject.prototype.notify = function (context) {
  let observerCount = this._observers.count();

  for (let i = 0; i < observerCount; i++) {
    this._observers.get(i).update(context);
  }
};

module.exports = Subject;