/**
 * Created by vlad on 10.3.17.
 */

'use strict';

const Observer = require('./observer');

function Display (options, extension) {
  this._name = options.name || 'NONE';
}

Display.prototype = Object.create(Observer.prototype);
Display.prototype.constructor = Display;

module.exports = Display;
