/**
 * Created by vlad on 10.3.17.
 */

'use strict';

function Observer() {}

Observer.prototype.update = function (value) {
  console.log(`${this._name}`, value);
};

module.exports = Observer;