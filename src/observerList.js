/**
 * Created by vlad on 10.3.17.
 */

'use strict';

function ObserverList () {
  this._observerList = [];
}

ObserverList.prototype.add = function (observer) {
  return this._observerList.push(observer);
};

ObserverList.prototype.count = function () {
  return this._observerList.length;
};

ObserverList.prototype.get = function (index) {
  if (index > -1 && index < this._observerList.length) {
    return this._observerList[index];
  }

  return false;
};

ObserverList.prototype.indexOf = function (observer, startIndex) {
  let index = startIndex;

  while (index < this._observerList.length) {
    if (this._observerList[index] === observer)
      return index;

    index++;
  }

  return -1;
};

ObserverList.prototype.removeAt = function (index) {
  this._observerList.splice(index, 1);
};

module.exports = ObserverList;